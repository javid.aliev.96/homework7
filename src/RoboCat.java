public class RoboCat extends Pet implements Foul {
    public RoboCat(String nickname, int ageOfPet, int trickLevel, String[] habits) {
        super(nickname, ageOfPet, trickLevel, habits);
        super.setSpecies(Species.RoboCat);
    }

    @Override
    public void respond() {
        System.out.println("Hello owner. I am a Robo-Cat and my name is " + getNickname() + ". I miss you!");
    }

    @Override
    public void foul() {
        System.out.println("RoboCat created a mess, RoboCat needs to cover it up");
    }
}
