import java.util.Arrays;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;

    }

    public Human(String name, String surname, Family family, int year) {
        this.name = name;
        this.surname = surname;
        this.family = family;
        this.year = year;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSurname() {

        return surname;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public int getYear() {

        return year;
    }

    public void setYear(int year) {

        this.year = year;
    }

    public int getIq() {

        return iq;
    }

    public void setIq(int iq) {

        this.iq = iq;
    }

    public String[][] getSchedule() {

        return schedule;
    }

    public void setSchedule(String[][] schedule) {

        this.schedule = schedule;
    }

    public Family getFamily() {

        return family;
    }

    public void setFamily(Family family) {

        this.family = family;
    }

    public void greetPet() {

        System.out.println("Hello, " + family.getPet().getNickname());
    }

    public void describePet() {
        if (family.getPet().getTrickLevel() > 50)
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is very sly.");
        else {
            System.out.println("I have a " + family.getPet().getSpecies() + ", he is " + family.getPet().getAgeOfPet() + " years old, he is almost not sly.");
        }
    }

    public boolean feedPet(boolean time) {
        boolean feeding = false;
        Random random = new Random(101);
        if (time == true) {
            feeding = true;
        } else if (family.getPet().getTrickLevel() >= random.nextInt()) {
            System.out.println("Hm...I will feed " + family.getPet().getNickname());
            feeding = true;
        } else {
            System.out.println("I think " + family.getPet().getNickname() + " is not hungry.");
            feeding = false;
        }
        return feeding;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
