public final class Woman extends Human {
    private Pet pet;

    public Woman(String name, String surname, int year, Pet pet) {
        super(name, surname, year);
        this.pet = pet;
    }

    public void whine() {
        System.out.println("We need to talk");
    }


    @Override
    public void greetPet() {
        System.out.println("Hello my dear " + this.pet.getNickname() + " I missed you a lot!");
    }
}
