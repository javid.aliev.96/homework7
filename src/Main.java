
public class Main {
    public static void main(String[] args) {
        Dog rex = new Dog("Rex", 5, 55, new String[]{"eat, nap, run"});
        rex.respond();
        rex.foul();
        rex.eat();
        System.out.println(rex.toString());

        Man john = new Man("John", "Test", 1970, rex);
        john.fedUp();
        john.greetPet();

        Woman anna = new Woman("Anna", "Test", 1971, rex);
        anna.whine();
        anna.greetPet();

        Family testFamily = new Family(anna, john);
        testFamily.setPet(rex);

        Man childSon = new Man("Son", "Test1", 1996, rex);
        childSon.greetPet();
        childSon.fedUp();

        testFamily.addChild(childSon);

        Woman childDaughter = new Woman("Daughter", "Test1", 1997, rex);
        childDaughter.whine();
        childDaughter.greetPet();

        testFamily.addChild(childDaughter);


    }
}




