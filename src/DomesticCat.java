public class DomesticCat extends Pet implements Foul {
    public DomesticCat(String nickname, int ageOfPet, int trickLevel, String[] habits) {
        super(nickname, ageOfPet, trickLevel, habits);
        super.setSpecies(Species.DomesticCat);
    }


    @Override
    public void foul() {
        System.out.println("Kitty created a mess, Kitty needs to cover it up");
    }

    @Override
    public void respond() {
        System.out.println("Hello owner. I am a Domestic Cat and my name is " + getNickname() + ". I miss you!");
    }
}
