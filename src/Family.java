import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Pet pet;

    public List getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List childrenList) {
        this.childrenList = childrenList;
    }

    private List childrenList;

    public Family(Human mother, Human father) {
        setMother(mother);
        setFather(father);
        this.childrenList = new ArrayList();
    }

    public Family() {
    }

    public Human getMother() {

        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {

        return father;
    }

    public void setFather(Human father) {

        this.father = father;
    }

    public Pet getPet() {

        return pet;
    }

    public void setPet(Pet pet) {

        this.pet = pet;
    }

    public void addChild(Human human) {

        this.childrenList.add(human);
    }

    public boolean deleteChild(int index) {
        boolean deletion;
        try {
            this.childrenList.remove(index);
            deletion = true;
        } catch (Exception exception) {
            System.out.println("Your required child is not correct");
            deletion = false;
        }
        return deletion;
    }

    public boolean deleteChild(Human human) {
        boolean deletion = false;
        if (this.childrenList.contains(human)) {
            this.childrenList.remove(human);
            return deletion = true;
        } else {
            return deletion = false;
        }
    }

    public int countFamily() {
        int count = 2;
        count += this.childrenList.size();
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) && Objects.equals(getFather(), family.getFather()) && Objects.equals(getPet(), family.getPet()) && Objects.equals(childrenList, family.childrenList);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Objects.hashCode(childrenList);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", pet=" + pet +
                ", childrenList=" + childrenList +
                '}';
    }
}
